// console.log("Hello World");

/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
	//first function here:

    let promptUserDetails = function(){
        let fullName = prompt("Enter your Full Name:");
        console.log(`Hello, ${fullName}`);
        let age = prompt("Enter your Age:");
        console.log(`You are ${age} years old.`);
        let location = prompt("Enter your Location:");
        console.log(`You live in ${location}`);
    }
    promptUserDetails();
/*

	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	//second function here:
    let showTop5Bands = function(){
        let top5Bands = ["Cocomelon", "Mayonaise", "Ichiworms", "Silverstein", "Bamboo"]
        for (let index = 0; index < top5Bands.length; index++) {
            console.log(`${index + 1}. ${top5Bands[index]}`);
        }
    }
    showTop5Bands();
/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	//third function here:
    let showTop5Movies = function(){
        let top5Movies = {
            movieNames : ["The Fabelmans", "The Menu", "Bones and All", "She Said", "Blank Panther: Wakanda Forever"],
            movieRatings : ["95%", "91%", "88%", "87%", "84%"] 
        }
            for (let indexMovies = 0; indexMovies < top5Movies.movieNames.length; indexMovies++) {
                console.log(`${indexMovies + 1}. ${top5Movies.movieNames[indexMovies]}`);
                console.log(`${top5Movies.movieNames[indexMovies]} Rating: ${top5Movies.movieRatings[indexMovies]}`);
                }
            }
    
    showTop5Movies();
/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/

let printFriends = function printUsers(){
	alert("Hi! Please add the names of your friends.");
	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");

	console.log("You are friends with:")
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
};
printFriends();


// console.log(friend1);
// console.log(friend2);
// console.log(friend3);
